const express = require('express')
const bodyParser = require('body-parser')
var config = require('./config')
const app = express()
const port = config.appSettings.port
const db = require('./queries')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json())

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/terms', db.getTerms)
app.post('/addterm', db.addTerm)
app.post('/suggestterm', db.suggestTerm)
app.get('/getsuggestions', db.getSuggestions)
app.post('/rejectsuggestion',db.rejectSuggestion)
app.get('/gettermbyvalue', db.getTermByValue)
app.post('/updatetermbyid', db.updateTermById)
app.get('/login', db.verifyUser)
app.post('/deletetermbyid',db.deleteTermById)
app.get('/getinactivesuggestionbyvalue', db.getInactiveSuggestionByValue)
app.post('/adduser', db.addUser)
app.post('/changeuserpassword', db.changeUserPassword)
app.post('/deleteuser', db.deleteUser)
app.get('/getuserbyusername', db.getUserByUsername)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API', Application: config.appName, Version: config.version })
  })

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })

app.use((req, res) => {
  console.log(req.body); // this is what you want
  res.on("finish", () => {
    console.log(res);  
  });
});