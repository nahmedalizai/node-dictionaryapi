const uuidV4 = require('uuid/v4');
var config = require('./config');
var bcrypt = require('bcryptjs');
const Pool = require('pg').Pool;
const fs = require('fs');

const pool = new Pool({
  user: config.db.username,
  host: config.db.host,
  database: config.db.dbName,
  password: config.db.password,
  port: config.db.port,
});

function getFormatedDate() {
  var date = new Date();
  var yyyy = date.getFullYear();
  var MM = date.getMonth();
  MM = MM+1;
  if (MM < 10)
    MM = '0'+ MM;
  var DD = date.getDate();
  if(DD < 10)
    DD = '0'+ DD;

  return (yyyy + '' + MM + '' + DD);
}

function logError(error) {
  var date = new Date();
  var fileName = 'errorlog_'+getFormatedDate()+date.getSeconds()+date.getMilliseconds()+'.txt';
  var errorMessage = 'Error Code & timepstamp: ' + error.code + ' - ' + date +  ', Message : ' + error.message + ', Error Stack : ' + error.stack;
  fs.writeFile(fileName, errorMessage, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("Error saved into the file : " + fileName);
  }); 
}

function logResults(results) {
var date = new Date();
var fileName = 'errorlog_'+getFormatedDate()+date.getSeconds()+date.getMilliseconds()+'.txt';
var errorMessage = results;
fs.writeFile(fileName, errorMessage, function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("Results saved into the file : " + fileName);
}); 
}

const getTerms = (request, response) => {
  pool.query('SELECT t.vrp_abv,t.vrp_term, t.vrp_description, c.vrp_categoryname ' +
              'FROM terms t, categories c ' + 
              'WHERE t.vrp_categoryid = c.vrp_categoryid ' +
              'ORDER BY t.vrp_abv ASC', (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(200).json(results.rows)
  })
}

const getTermByValue = (request, response) => {
  pool.query('SELECT DISTINCT t.id, t.vrp_term, t.vrp_abv, t.vrp_description, c.vrp_categoryname, c.vrp_categoryid ' + 
              'FROM terms t, categories c ' +
              'WHERE t.vrp_categoryid = c.vrp_categoryid ' +
              'AND (t.vrp_abv = $1 OR t.vrp_term = $1)',[request.query.value], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(200).json(results.rows)
  })
}

const getInactiveSuggestionByValue = (request, response) => {
  pool.query('SELECT id, vrp_term, vrp_description, vrp_email, vrp_username, vrp_addedon, vrp_isadded, vrp_abbrev, vrp_isaccepted ' +
             'FROM suggestions ' +
             'WHERE vrp_isaccepted = false AND (vrp_abbrev = $1 OR vrp_term = $1)',[request.query.value], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(200).json(results.rows)
  })
}

const updateTermById = (request, response) => {
  pool.query('UPDATE terms SET ' +
              'vrp_term = $1, vrp_abv = $2, vrp_description = $3, vrp_categoryid = $4 ' +
              'WHERE id = $5',[request.query.vrp_term, request.query.vrp_abv, request.query.vrp_description, request.query.vrp_categoryid, request.query.id], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(201).send(`Term updated`)
  })
}

const deleteTermById = (request, response) => {
  pool.query('DELETE FROM terms WHERE id =  $1 ' ,[request.query.id], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    if(results.rowCount > 0) {
      response.status(201).send(`Term deleted`)
    }
    else
      response.status(202).send(`Term does not exist`)
  })
}

const addUser = (request, response) => {
  // var salt = bcrypt.genSaltSync(10);
  // var hash = bcrypt.hashSync(request.query.vrp_password, bcrypt.genSaltSync(10));
  // console.log('Plain password : ' + request.query.vrp_password);
  // console.log('Salt : ' + bcrypt.genSaltSync(10));
  // console.log('Hashed and salted password : ' + hash);
  // console.log('Result : ' + bcrypt.compareSync(request.query.vrp_password, hash));
  pool.query('SELECT COUNT(id) FROM users WHERE vrp_username=$1' ,[request.query.vrp_username.toUpperCase()], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    if(results.rows[0].count === '0') {
      pool.query('INSERT INTO public.users( vrp_name, vrp_username, vrp_userid, vrp_password) VALUES ($1, $2, $3, $4)' ,
      [request.query.vrp_name, request.query.vrp_username.toUpperCase(),uuidV4(),bcrypt.hashSync(request.query.vrp_password, bcrypt.genSaltSync(10))], (error, results) => {
        if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please check server logs')
        }
        response.status(201).send(`User added`)
      })      
    }
    else
      response.status(202).send(`Username already exists`)
  })
}

const verifyUser = (request, response) => {
  if(request.query.username === config.adminUser.username && request.query.password === config.adminUser.password)
    response.status(200).send({isSuccess: true})
  else {
    pool.query('SELECT vrp_password FROM users WHERE vrp_username=$1',[request.query.username.toUpperCase()], (error, results) => {
      if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please check server logs')
        }
      if(results.rows.length > 0) {
        if(bcrypt.compareSync(request.query.password, results.rows[0].vrp_password))
          response.status(200).send({isSuccess: true})
        else
          response.status(202).send({isSuccess: false}) 
      }
      else
        response.status(202).send({isSuccess: false})
    })
  }
}

const getSuggestions = (request, response) => {
  pool.query('SELECT id, vrp_term,vrp_abbrev, vrp_description, vrp_email FROM suggestions WHERE vrp_isadded = false AND vrp_isaccepted=true ORDER BY id ASC', (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(200).json(results.rows)
  })
}

const addTerm = (request, response) => {
  if(request.query.data)
  {
    var row = JSON.parse(request.query.data);
    pool.query('SELECT id, vrp_termid FROM terms WHERE vrp_abv = $1',[row.vrp_abbrev], (error, results) => {
      if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please check server logs')
      }
      if(results.rows.length > 0)
        response.status(202).send(`Term already exists`)
      else {
        pool.query('INSERT INTO terms (vrp_termid, vrp_abv, vrp_term, vrp_description, vrp_categoryid, vrp_addedby, vrp_addedon)' + 
          'VALUES ($1, $2, $3, $4, $5, $6, $7)', [uuidV4(),row.vrp_abbrev, row.vrp_term, row.vrp_description, row.vrp_categoryid, '123e4567-e89b-12d3-a456-000000000000',getFormatedDate()], (error, results) => {
            if (error) {
              logError(error)
              response.status(500).send('An error occured in the API, please check server logs')
            }
              pool.query('UPDATE suggestions SET vrp_isadded=true where vrp_term = $1', [row.vrp_term], (error, results) => {
                if (error) {
                  logError(error)
                  response.status(500).send('An error occured in the API, please check server logs')
                }
              //response.status(201).send(`Term added with ID:`)
              })
            response.status(201).send(`Term added`)
          })
      }
    })
  }      
    // var row = null;
    // var jsonObj = request.query.data;
    // jsonObj.forEach(element => {
    //   row = JSON.parse(element);
    //   pool.query('INSERT INTO terms (vrp_termid, vrp_abv, vrp_term, vrp_description, vrp_categoryid, vrp_addedby, vrp_addedon)' + 
    //   'VALUES ($1, $2, $3, $4, $5, $6, $7)', [uuidV4(),row.vrp_abbrev, row.vrp_term, row.vrp_description, row.vrp_categoryid, '123e4567-e89b-12d3-a456-000000000000',getFormatedDate()], (error, results) => {
    //     if (error) {
    //     throw error
    //     }
    //     //response.status(201).send(`Term added`)
    //     })
    //     pool.query('UPDATE suggestions SET vrp_isadded=true where vrp_term = $1', [row.vrp_term], (error, results) => {
    //       if (error) {
    //         throw error
    //       }
    //       //response.status(201).send(`Term added with ID:`)
    //     })
    // });
    // response.status(201).send([]);
}

const rejectSuggestion = (request, response) => {
  if(request.query.id)
  {
    pool.query('SELECT * FROM suggestions WHERE id = $1',[request.query.id], (error, results) => {
      if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please check server logs')
      }
      //console.log(results)
      if(results.rowCount > 0)
      {
        pool.query('UPDATE suggestions SET vrp_isaccepted=false where id = $1', [request.query.id], (error, results) => {
          if (error) {
            logError(error)
            response.status(500).send('An error occured in the API, please check server logs')
          }
          response.status(201).send(`Found Records and Rejected : ` + results.rowCount)
        })
      }
      else
        response.status(202).send(`No record found with Id ` + request.query.id)
    })
  }    
}


const suggestTerm = (request, response) => {
  //const { term, description, email } = request.body
  var isSuccess = true;
  pool.query('INSERT INTO suggestions (vrp_abbrev, vrp_term, vrp_description, vrp_email, vrp_username, vrp_addedon, vrp_isadded, vrp_isaccepted)' + 
              'VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [request.query.abrev, request.query.term, request.query.description, request.query.email, request.query.username, getFormatedDate(), false, true], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(201).send(isSuccess)
  })
}

const changeUserPassword = (request, response) => {
  pool.query('UPDATE users SET vrp_password=$1 WHERE id =$2', 
              [bcrypt.hashSync(request.query.password, bcrypt.genSaltSync(10)), request.query.id], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    if(results.rowCount > 0) {
      response.status(201).send('User password updated')
    }
    else
      response.status(202).send('Updating user password failed')
  });
}

const getUserByUsername = (request, response) => {
  //const { term, description, email } = request.body
  var isSuccess = true;
  pool.query('SELECT id, vrp_name, vrp_username FROM users WHERE vrp_username=$1', [request.query.username.toUpperCase()], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    response.status(200).json(results.rows)
  })
}

const deleteUser = (request, response) => {
  pool.query('DELETE FROM users WHERE id = $1', [request.query.id], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please check server logs')
    }
    if(results.rowCount > 0) {
      response.status(201).send('User deleted')
    }
    else
      response.status(202).send('User deletion failed')
  });
}

module.exports = {
  getTerms,
  addTerm,
  suggestTerm,
  getSuggestions,
  rejectSuggestion,
  getTermByValue,
  updateTermById,
  verifyUser,
  deleteTermById,
  getInactiveSuggestionByValue,
  addUser,
  changeUserPassword,
  getUserByUsername,
  deleteUser
}